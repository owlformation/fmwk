Réalisez les exercices suivant à l'aide de la doc Bulma ( utilisez google traduction si besoin)

# Exercice  1 : Mise en route !
Précisions :
- Section pleine hauteur colorée
- Titre + sous-titre au centre de l'écran
- 4 collumns avec un gap

# Exercice 2 : NavBar

Ajoute une navbar selon les conditions suivantes :
- Le menu "dropdown" est à droite
- Le menu "dropdown" n'a pas de flèche
- Le menu "dropdown" a les bords du bas arrondis
- Les items du menu "dropdown" ont tous un séparateur
- Le logo à gauche

# Exercice 3 : Nested collumns

Précisions :
- Section pleine hauteur colorée
- Contenu centré
- image 4x3, pour l'image vous êtes libre, je recommande [ce site](https://picsum.photos/)
- Partie gauche : une collumn
- Partie droite : deux collumn l'une sous l'autre

# Exercice 4 : Site complet !

On attaque les choses serieuses ! Le client vous demande de réaliser un
site selon son cahier des charges, tu relèves le défi ? (en vrai tu n'as pas le choix  ...)

## CAHIER DES CHARGES

### Précisions :

Le site doit être réalisé uniquement avec bulma et du HTML.
Le site sera sur le thème du "voyage" .
Tu n'auras pas d'aide sur le design, tu es totalement libre !
Bien évidement chaque page aura une navBar et un footer !
Respecte juste le contenu demandé ci-dessous.

Le site contiendra les pages suivantes :

### Accueil
- Section 1 : Un grand titre et une section pleine hauteur
- Section 2 : Une image et un texte de présentation (le lorem est autorisé)
- Section 3 : Trois images clickables alignées qui redirigent sur une "Page voyage"
- Section 4 : Un formulaire de contact, propose celui qui te semble le plus pertinant

### Page voyage x 3
- Section 1 : Un grand titre et une section pleine hauteur
- Section 2 : Fais rêver le client
- Section 3 : Fais rêver le client
- Section 4 : Une section commentaire avec des vrais/faux commentaires si possible !

### Page contact
- Section 1 : Un grand titre et une section pleine hauteur
- Section 2 : Un formulaire de contact, propose celui qui te semble le plus pertinant



# Exercice 5 : On prend (presque) les mêmes et on recommence !

Réalise les exercices 1, 2, 3 et 4 avec 2 autres frameWorks !
